#!/usr/bin/env python3

import argparse
import utils
from EnvDefault import env_default


def main(options):

    rds = utils.RDS(options.region)

    fail_not_found = True
    rds.delete_db_instance(options.region, options.instance_name, fail_not_found)

if __name__ == "__main__":

    p = argparse.ArgumentParser(description='RDS-CLI - this will delete a database instance so be careful!!!.')
    p.add_argument("-r", "--region",        action=env_default('REGION'),           required=True, help="Specify the aws region in full form : ap-southeast-2")
    p.add_argument("-n", "--instance_name", action=env_default('INSTANCE_NAME'),    required=True, help="Specify name of the instance to delete")
    options = p.parse_args()

    main(options)



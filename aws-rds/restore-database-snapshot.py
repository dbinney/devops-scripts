#!/usr/bin/env python3


import argparse
import utils
from EnvDefault import env_default


def main(options):

    rds = utils.RDS(options.region)

    fail_not_found = False
    rds.delete_db_instance(opts.region, opts.dest_instance_name, fail_not_found)



    rds.create_database_from_snapshot(
        opts.source_instance_name, 
        opts.dest_instance_name, 
        opts.db_instance_class, 
        opts.db_subnet, 
        opts.vpc_security_group_ids, 
        opts.master_user_password, 
        opts.db_parameter_group_name
    )
    

if __name__ == "__main__":
    import argparse

    p = argparse.ArgumentParser(description='RDS-CLI - this will restore an rds db from the last avail snapshot.')

    # needed for base client
    p.add_argument("-r", "--region",                action=env_default('REGION'),                    required=True, help="Specify the aws region in full form : ap-southeast-2")
    
    # needed for new instance and vpc etc 
    p.add_argument("-s", "--source_instance_name",   action=env_default("SOURCE_INSTANCE_NAME"),     required=True, help="Source db instance name to find snapshot for.")
    p.add_argument("-d", "--dest_instance_name",     action=env_default("DEST_INSTANCE_NAME"),       required=True, help="Dest db instance name for the new snapshot")
    p.add_argument("-c", "--db_instance_class",      action=env_default("DB_INSTANCE_CLASS"),        required=True, help="RDS instance class i.e t2.small")
    p.add_argument("-b", "--db_subnet",              action=env_default("DB_SUBNET"),                required=True, help="Instance subnet group")
    p.add_argument("-v", "--vpc_security_group_ids", action=env_default("VPC_SECURITY_GROUP_IDS"),   required=True, help="VPC security group id's which becomes an array/list and seperate with spaces", nargs='+')
    p.add_argument("-p", "--master_user_password",   action=env_default("MASTER_USER_PASSWORD"),     required=True, help="Master postgres user password")
    p.add_argument("-g", "--db_parameter_group_name",action=env_default("DB_PARAMETER_GROUP_NAME"),  required=True, help="DB parameter group name ")




    opts = p.parse_args()
    print(opts)

    main(opts)



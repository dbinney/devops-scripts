#!/usr/bin/env python3

import boto3
import sys
import re
import datetime
import logging
import time
import argparse
import os


class RDS:
    
    protected_dbs = set(["asdfadf","postgres-testing","sinecloud-postgres"])

    def __init__(self, region):
        try:
            self.rds_client = boto3.client('rds', region_name=region)
        except botocore.exceptions.ClientError as e:
            raise Exception("Could not connect to rds: %s" %e)
 

    def create_database_from_snapshot(self, source_instance_name, dest_instance_name, db_instance_class, db_subnet, vpc_security_group_ids, master_user_password, db_parameter_group_name):
        ### ready for creation
        try:
            rds_client_snaps = self.rds_client.describe_db_snapshots(DBInstanceIdentifier = source_instance_name)['DBSnapshots']
            rds_client_snap = sorted(rds_client_snaps, key=RDS.byTimestamp, reverse=True)[0]['DBSnapshotIdentifier']
            # dest_instance_name = (re.sub( '(.*\:)(.*)(-\d{4}-\d{2}-\d{2}-\d{2}.*', '$2', rds_client_snap))


            # print(rds_client_snaps)
            # print(rds_client_snap)

            print('Restoring %s to %s' % (rds_client_snap, dest_instance_name))
            self.rds_client.restore_db_instance_from_db_snapshot(
                DBInstanceIdentifier=dest_instance_name,
                DBSnapshotIdentifier=rds_client_snap,
                DBInstanceClass=db_instance_class,
                DBSubnetGroupName=db_subnet,
                MultiAZ=False,
                PubliclyAccessible=True
            )
            RDS.waiting_available(self, dest_instance_name, "creating db and backups")

        except botocore.exceptions.ClientError as e:
            raise Exception("Error occured while restoring: %s" % e)

        ### modify instance after creation.
        try:
            response = self.rds_client.modify_db_instance(
                DBInstanceIdentifier=dest_instance_name,
                VpcSecurityGroupIds=vpc_security_group_ids,
                ApplyImmediately=True,
                MasterUserPassword=master_user_password,
                DBParameterGroupName=db_parameter_group_name,
                BackupRetentionPeriod=0
            )
            RDS.waiting_available(self, dest_instance_name, "modify database")

            response = self.rds_client.reboot_db_instance(
                DBInstanceIdentifier=dest_instance_name
            )
            RDS.waiting_available(self, dest_instance_name, "restarting")

        except botocore.exceptions.ClientError as e:
            raise Exception("Could not modify instance: %s" % e)

    def delete_db_instance(self, region, instance_name, fail_not_found):
        

        if not RDS.does_db_exist(self, region, instance_name):
            print("no db there sorry")

            if fail_not_found:
                print("ERROR: exiting, could not find your Db: instance_name")
                exit()

            

        if instance_name in self.protected_dbs or not self.protected_dbs:
            # raise Exception("You are attempting to blow away a protected db.. exiting: %s" %instance_name)
            print("You are attempting to blow away a protected db.. exiting: %s" %instance_name)

        # use wait just in case state is changing during run.. i.e still creating
        RDS.waiting_available(self, instance_name, "pre-emptive wait just in case you run too often")

        try:
            self.rds_client.delete_db_instance(
                DBInstanceIdentifier=instance_name,
                SkipFinalSnapshot=True
            )
            RDS.waiting_deleted(self, instance_name, "deleting instance")
        except botocore.exceptions.ClientError as e:
            if(fail_not_found):
                print("DB delete failure")
                # raise Exception("Could not delete instance: %s" % e)
            else:
                print("skipping delete, db does not exist -> %s" %instance_name)
        except:
            print("Unexpected error:", sys.exc_info()[0])
            raise



    def does_db_exist(self, region, instance_name):

        rds_client = boto3.client('rds', region_name=region)

        try:
            rds_client.describe_db_instances(DBInstanceIdentifier=instance_name)
            return True
        except botocore.exceptions.ClientError:
                return False
        # raise Exception("Could not delete instance: %s" % e)



    def waiting_available(self, db_identifier, message):

            try:
                rds_waiter = self.rds_client.get_waiter('db_instance_available')
                print("waiting.. 30 second intervals - %s" %message)
                rds_waiter.wait(DBInstanceIdentifier=db_identifier)
            except botocore.exceptions.WaiterError as e:
                print("Database does not exist or is currently being deleted:")

    def waiting_deleted(self, db_identifier, message):
            try:
                rds_waiter = self.rds_client.get_waiter('db_instance_deleted')
                print("waiting.. 30 second intervals - %s" %message)
                rds_waiter.wait(DBInstanceIdentifier=db_identifier)
            except botocore.exceptions.WaiterError as e:
                print("Not sure what happened here %s" %e)

    def byTimestamp(snap):
        item = snap
        if 'SnapshotCreateTime' in snap:
            return datetime.datetime.isoformat(snap['SnapshotCreateTime'])
        else:
            return datetime.datetime.isoformat(datetime.datetime.now())

if __name__ == "__main__":
    pass


